package libnmdc

import (
	"testing"
)

func TestMyINFOParse(t *testing.T) {

	np := NewNmdcProtocol(nil).(*NmdcProtocol)

	type myInfoTestPair struct {
		in     string
		expect UserInfo
	}

	cases := []myInfoTestPair{

		myInfoTestPair{
			in: "$ALL Bxxxy description<ApexDC++ V:1.4.3,M:P,H:9/0/2,S:1>$ $0.01\x01$xyz@example.com$53054999578$",
			expect: UserInfo{
				Nick:             "Bxxxy",
				Description:      "description",
				ClientTag:        "ApexDC++",
				ClientVersion:    "1.4.3",
				Email:            "xyz@example.com",
				ShareSize:        53054999578,
				Flag:             FLAG_NORMAL,
				Slots:            1,
				HubsUnregistered: 9,
				HubsRegistered:   0,
				HubsOperator:     2,
				UserInfo_NMDCOnly: UserInfo_NMDCOnly{
					ConnectionMode: CONNECTIONMODE_PASSIVE,
					Speed:          "0.0",
				},
			},
		},
		myInfoTestPair{
			in: "$ALL ixxxxxxx0 $P$10A$$0$",
			expect: UserInfo{
				Nick:          "ixxxxxxx0",
				ClientVersion: "0", // Auto-inserted by the parser for short-format MyINFO strings
				Flag:          UserFlag(rune('A')),
				UserInfo_NMDCOnly: UserInfo_NMDCOnly{
					ConnectionMode: CONNECTIONMODE_PASSIVE,
					Speed:          "1",
				},
			},
		},
		myInfoTestPair{
			in: "$ALL SXXXX_XXXXXXR <ncdc V:1.19.1-12-g5561,M:P,H:1/0/0,S:10>$ $0.005Q$$0$",
			expect: UserInfo{
				Nick:             "SXXXX_XXXXXXR",
				ClientTag:        "ncdc",
				ClientVersion:    "1.19.1-12-g5561",
				Flag:             UserFlag(rune('Q')),
				Slots:            10,
				HubsUnregistered: 1,
				UserInfo_NMDCOnly: UserInfo_NMDCOnly{
					ConnectionMode: CONNECTIONMODE_PASSIVE,
					Speed:          "0.00",
				},
			},
		},
		myInfoTestPair{
			in: "$ALL mxxxu desccccc<HexChat V:2.12.1,M:P,H:1/0/0,S:0>$ $p$$0$",
			expect: UserInfo{
				Nick:             "mxxxu",
				Description:      "desccccc",
				ClientTag:        "HexChat",
				ClientVersion:    "2.12.1",
				Flag:             UserFlag(rune('p')),
				HubsUnregistered: 1,
				Slots:            0,
				UserInfo_NMDCOnly: UserInfo_NMDCOnly{
					ConnectionMode: CONNECTIONMODE_PASSIVE,
				},
			},
		},
	}

	for _, v := range cases {

		got, err := np.parseMyINFO(v.in)

		if err != nil {
			t.Errorf("MyINFO parse warning (%s)", err.Error())
			continue
		}

		if *got != v.expect {
			t.Errorf("MyINFO parse failure\nExpected:\n%+v\nGot:\n%+v\n", v.expect, got)
			continue
		}
	}

}
