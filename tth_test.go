package libnmdc

import (
	"strings"
	"testing"
)

func TestTTH(t *testing.T) {

	// echo -n 'hello world' | tthsum
	testCases := [][2]string{
		[2]string{"hello world", "ZIIVRZDR2FD3W4KKNMNYUU3765LPPK7BWY64CHI"},
		[2]string{"", "LWPNACQDBZRYXW3VHJVCJ64QBZNGHOHHHZWCLNQ"},
		[2]string{"\x00", "VK54ZIEEVTWNAUI5D5RDFIL37LX2IQNSTAXFKSA"},
		[2]string{strings.Repeat("A", 1024), "L66Q4YVNAFWVS23X2HJIRA5ZJ7WXR3F26RSASFA"},
	}

	short := func(s string) string {
		if len(s) > 15 {
			return s[0:15] + "..."
		}
		return s
	}

	for _, testCase := range testCases {
		input, expected := testCase[0], testCase[1]
		result, err := TTH(input)
		if err != nil {
			t.Fatalf("Error getting TTH for '%s': %s", short(input), err.Error())
		}

		if Base32(result) != expected {
			t.Fatalf("Wrong TTH for '%s' (got '%s' expected '%s')", short(input), result, expected)
		}
	}
}
