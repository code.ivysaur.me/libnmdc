package libnmdc

import (
	"fmt"
)

type ConnectionMode rune

const (
	CONNECTIONMODE_ACTIVE  ConnectionMode = 'A' // 65
	CONNECTIONMODE_PASSIVE ConnectionMode = 'P' // 49
	CONNECTIONMODE_SOCKS5  ConnectionMode = '5' // 53
)

func (this ConnectionMode) String() string {
	switch this {
	case CONNECTIONMODE_ACTIVE:
		return "Active"

	case CONNECTIONMODE_PASSIVE:
		return "Passive"

	case CONNECTIONMODE_SOCKS5:
		return "SOCKS5"

	default:
		return fmt.Sprintf("ConnectionMode(\"%s\")", string(this))
	}
}
