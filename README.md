# libnmdc

An NMDC / ADC client protocol library for Golang.

Install via go get: `go get code.ivysaur.me/libnmdc`

## Features

- Connect to NMDC and ADC hubs
- SSL (NMDCS/ADCS) with option to ignore certificate validity
- Autodetect NMDC/ADC protocol by timeout
- Send public and private chat messages, UserCommand support
- Protocol keepalives
- Parse user details (including UserIP2 for NMDC)
- Fast NMDC login via NoHello and QuickList
- Both synchronous (callback) and asynchronous (channel) -based APIs, including example

## Changelog

2018-12-31 0.18.0
- BREAKING: Return error from more functions
- Feature: Add new `EVENT` constant to detect connection failures caused by bad password, hub full, etc
- Convert to Go Modules

2018-03-24 0.17.0
- Feature: Re-add sayInfo() function for reannouncing MyINFO changes

2017-11-26 0.16.0
- Feature: Support connecting to ADC hubs
- BREAKING: Simplify connection API
- Vendor new dependency on github.com/cxmcc/tiger (MIT license)

2017-11-14 0.15.0
- Feature: Fallback reconnection if no data (not even keepalives) are recieved from the hub in 24 hours
- Fix an issue with detecting protocol messages inside multi-line chat messages
- Update examples and the default client version number

2017-02-09 0.14.0
- Fix an issue with crashing on malformed IP addresses supplied by the hub

2017-02-09 0.13.0
- Feature: Implement UserIP2 extension, to retrieve IP addresses of other users
- Enhancement: Implement QuickList extension (reduce one network roundtrip during initial connection)
- Enhancement: Implement NoHello extension (faster connection performance)
- Enhancement: Implement ChatOnly extension
- Fix an issue with not notifying client on all MyINFO updates

2017-02-05 0.12.0
- Fix an issue with mutex deadlock when accessing user information from a callback
- Fix an issue with silent disconnection if a password was required but not present

2016-11-29 0.11.0
- BREAKING: Remove some exported methods
- BREAKING: Fix an issue with missing sufficient parameters in the synchronous API
- Enhancement: Improve output under godoc
- Fix an issue with special characters appearing in recieved private messages
- Fix an issue with parsing active/passive connection modes
- Fix an issue with errors appearing on stdout

2016-10-08 0.10.0
- Feature: Support `$UserCommand`

2016-08-27 0.9.0
- Fix an issue with parsing MyINFO strings with zero-length speed descriptions
- Fix an issue with not storing updated profile information

2016-05-10 0.8.0
- Enhancement: Separate `ClientTag` and `ClientVersion` in `UserInfo` structs

2016-05-08 0.7.0
- BREAKING: Remove direct access to `HubConnection.Users` map
- Feature: Threadsafe user map accessor
- Feature: Option to disable auto-reconnection
- Feature: New `Disconnect()`, `UserCount()`, `UserExists()` functions
- Enhancement: Support `$OpList`, add `IsOperator` member to `UserInfo` structs
- Refactor into multiple files

2016-04-16 0.6.0
- Fix an issue with calling `panic()` on certain types of abnormal network failure

2016-04-04 0.5.0
- Enhancement: Support protocol keepalives
- Enhancement: Support hub redirects (`$ForceMove`)

2016-04-03 0.4.0
- Feature: Add synchronous API
- Fix an issue with reading HubConnection's state parameter
- Fix an issue with buffered protocol commands

2016-04-03 0.3.0
- Feature: Add `SkipVerifyTLS` option
- Fix an issue with calling `panic()` if connection failed

2016-04-02 0.2.0
- Enhancement: Support NMDC-over-TLS (NMDCS)
- Fix an issue recieving private messages
- Fix an issue with calling `panic()` if connection failed
- Fix an issue parsing URIs without a specified port
- Move sample content into directory with excluded build

2016-02-12 0.1.0
- Initial public release
