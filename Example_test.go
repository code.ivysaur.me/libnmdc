package libnmdc

import (
	"fmt"
)

func ExampleHubConnectionOptions_Connect() {
	opts := HubConnectionOptions{
		Address: "127.0.0.1",
		Self:    NewUserInfo("slowpoke9"),
	}

	events := make(chan HubEvent, 0)
	hub := ConnectAsync(&opts, events)

	for event := range events {
		switch event.EventType {
		case EVENT_CONNECTION_STATE_CHANGED:
			fmt.Printf("Connection -- %s (%s)\n", event.StateChange, event.Message)

		case EVENT_PUBLIC:
			fmt.Printf("Message from '%s': '%s'\n", event.Nick, event.Message)
			if event.Message == "how are you" {
				hub.SayPublic("good thanks!")
			}

		default:
			fmt.Printf("%+v\n", event)

		}
	}
}

func ExampleHubConnectionOptions_ConnectSync() {
	cb := func(hub *HubConnection, event HubEvent) {
		switch event.EventType {
		case EVENT_CONNECTION_STATE_CHANGED:
			fmt.Printf("Connection -- %s (%s)\n", event.StateChange, event.Message)

		case EVENT_PUBLIC:
			fmt.Printf("Message from '%s': '%s'\n", event.Nick, event.Message)
			if event.Message == "how are you" {
				hub.SayPublic("good thanks!")
			}

		default:
			fmt.Printf("%+v\n", event)

		}
	}

	opts := HubConnectionOptions{
		Address: "127.0.0.1",
		Self:    NewUserInfo("slowpoke9"),
	}

	ConnectSync(&opts, cb) // blocking
}
